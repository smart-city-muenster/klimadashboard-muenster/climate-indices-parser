import json
import sys
from ClimateIndicesParser import ClimateIndicesParser


def parse(url):
    """parse a url and get records"""
    parsers = ClimateIndicesParser(url=url)
    parsers.download()
    records = list(parsers.parse())
    parsers.cleanup()
    return records


# get args from cmd (exclude the first one which is the script name)
urls = sys.argv[1::]

# get records from urls
data = [parse(url) for url in urls]

# flatted records
flat_data = [item for sublist in data for item in sublist]

# remove duplicate records by timestamp
reduced = list(
    {dictionary['timestamp']: dictionary for dictionary in flat_data}.values())

# sort records by timestamp
sortedlist = sorted(reduced, key=lambda d: d['timestamp'])

# create json from records list
json_list = json.dumps(sortedlist, indent=4, sort_keys=True,
                       default=str)

# write records
print(json_list)
