import datetime

from brightsky.parsers import ObservationsParser
from dateutil.tz import tzutc


class ClimateIndicesParser(ObservationsParser):
    elements = {
        'tropennaechte': 'MO_TROPENNAECHTE',
        'frosttage': "MO_FROSTTAGE",
        'sommertage': "MO_SOMMERTAGE",
        'heisse_tage': "MO_HEISSE_TAGE",
        'eistage': "MO_EISTAGE",
        "dwd_station_id": "STATIONS_ID"
    }

    def parse_station_id(self, zf):
        return None

    def parse_lat_lon_history(self, zf, dwd_station_id):
        return {}

    def parse_reader(self, filename, reader, lat_lon_history):
        for row in reader:
            timestamp = datetime.datetime.strptime(
                row['MESS_DATUM_BEGINN'], '%Y%m%d').replace(tzinfo=tzutc())
            yield {
                'timestamp': timestamp,
                **self.parse_elements(row, None, None, None),
            }
