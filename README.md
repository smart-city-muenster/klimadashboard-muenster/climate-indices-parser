# Climate Indices Parser

This python script reads climate indices from the [DWD (Deutscher Wetterdienst)](https://www.dwd.de/DE/Home/home_node.html) using the [Bright Sky](https://github.com/jdemaeyer/brightsky/) project.

### Usage

You need to provide URL(s) to the climate indices data (see [https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/monthly/climate_indices/kl/](https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/monthly/climate_indices/kl/)). You can choose recent and historical data. The application need the `.zip` URLs.

```sh
pip install -r requirements.txt
python index.py [climate_indices_urls]
```

**Example**
This example fetches historical data from the former weatherstation Münster and the new weatherstation Münster/Osnabrück as well as recent data from Münster/Osnabrück. Duplicate records (by timestamp) are removed.

```sh
python index.py https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/monthly/climate_indices/kl/historical/monatswerte_KLINDEX_01766_19891001_20231231_hist.zip https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/monthly/climate_indices/kl/historical/monatswerte_KLINDEX_03404_18910101_19911231_hist.zip https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/monthly/climate_indices/kl/recent/monatswerte_KLINDEX_01766_akt.zip > data/climate_indices.json
```

### Output

The script writes a new file `climate_indices.json` to the file system. The file will look like this:

```json
[
    {
        "dwd_station_id": 3404.0,
        "eistage": 15.0,
        "frosttage": 25.0,
        "heisse_tage": 0.0,
        "observation_type": "historical",
        "sommertage": 0.0,
        "timestamp": "1891-01-01 00:00:00+00:00",
        "tropennaechte": 0.0,
        "wmo_station_id": null
    },
    {
        "dwd_station_id": 3404.0,
        "eistage": 1.0,
        "frosttage": 25.0,
        "heisse_tage": 0.0,
        "observation_type": "historical",
        "sommertage": 0.0,
        "timestamp": "1891-02-01 00:00:00+00:00",
        "tropennaechte": 0.0,
        "wmo_station_id": null
    },
    ...
```

### Development

With the `.devcontainer` folder, you can immediately start using the script using a [VSCode Dev Container](https://code.visualstudio.com/docs/devcontainers/containers). You just need to install the dependencies (`pip install -r requirements.txt`)
